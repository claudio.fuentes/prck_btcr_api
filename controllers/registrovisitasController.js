var config = require('../dbconfig')
const sql = require('mssql')


/**
 * create a new visit register on the database
 * @param {any} req require parameters
 * @param {any} res response data
 */
async function nuevo(req,res){
    try {
        let pool = await sql.connect(config)
        
        let ret = await pool.request()
                    .input('horaInicio',sql.VarChar(7),req.body.horaInicio)
                    .input('horaTermino',sql.VarChar(7),req.body.horaTermino)
                    .input('especificacion',sql.VarChar(5000),req.body.especificaciones)
                    .input('solicitudes',sql.VarChar(5000),req.body.solicitudes)
                    .input('compras',sql.VarChar(5000),req.body.compras)
                    .input('observaciones',sql.VarChar(5000),req.body.observaciones)
                    .input('responsable_id',sql.Int,parseInt(req.body.responsable))
                    .input('asistente_id',sql.Int,parseInt(req.body.asistente))
                    .input('anfitrion_id',sql.Int,parseInt(req.body.anfitrion))
                    .input('sucursal_id',sql.Int,parseInt(req.body.sucursal))
            .execute('spIns_registroVisita')
       
        var codigoStatus = 500
        switch (ret.recordsets.CODE) {
            case 1:
                codigoStatus = 200
                break;
            case -1:
                codigoStatus = 400
                break;
            case -2:
                codigoStatus = 500
            break;
            default:
                codigoStatus = 500
                break;
        }
        console.log(JSON.stringify(ret));
        res.status(codigoStatus).json(ret.recordsets)
    } catch (err) {
        console.log(`error: ${JSON.stringify(err)}`);
        res.status(500).json({error: err})
    }
}

/**
 * lista all register to show a simple view (such a card)
 * @param {any} req require data
 * @param {any} res response data
 */
async function listaCard(req,res){
    try {
        let pool = await sql.connect(config)
        
        let ret = await pool.request()
            .execute('spRec_registroVisita_cards')
       
        var codigoStatus = 200
        res.status(codigoStatus).json(ret.recordsets[0])
    } catch (err) {
        console.log(`error: ${JSON.stringify(err)}`);
        res.status(500).json({error: err})
    }
}


async function registroVisitaInfo(req,res) {
    try {
        const {id} = req.query
        console.log(id)
        let pool = await sql.connect(config)
        let ret = await pool.request()
            .input('id',sql.Int,id)
            .execute('spRec_registroVisita')
       
        var codigoStatus = 200
        res.status(codigoStatus).json(ret.recordsets[0][0])
    } catch (err) {
        console.log(`error: ${JSON.stringify(err)}`);
        res.status(500).json({error: err})
    }
}
module.exports = {
    nuevo,
    listaCard,
    registroVisitaInfo
}