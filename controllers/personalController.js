var config = require('../dbconfig')
const sql = require('mssql')

async function listaSimple(req,res){
    try {
        let pool = await sql.connect(config)
        // let ret = await pool.request().query(`spRec_personal_simple ${parseInt(req.query.idempresa)}`)
        let ret = await pool.request().input("idEmpresa",sql.Int,req.query.idempresa).execute('spRec_personal_simple')
        

        res.status(200).json(ret.recordsets[0])
    } catch (err) {
        console.log(`error: ${err}`)
        res.status(500).json({error: err})
    }
}

module.exports = {
    listaSimple
}