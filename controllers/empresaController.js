var config = require('../dbconfig')
const sql = require('mssql')



async function listaSimple(req,res){
    try {
        let pool = await sql.connect(config)
        let ret = await pool.request().query(`EXEC spRec_empresa_Simple`)
        
        res.status(200).json(ret.recordsets[0])
    } catch (err) {
        console.log(`error: ${err}`);
        res.status(500).json({error: err})
    }
}


module.exports = {
    listaSimple
}