const { CreaToken } = require("../Seguridad")
var config = require('../dbconfig')
const sql = require('mssql')


async function validaUsuario(req, res) {
    try {
        let pool = await sql.connect(config)
        let ret = await pool.request()
            .input("usuario", sql.VarChar(150), req.body.usuario)
            .input("contrasenna", sql.VarChar(250), req.body.contrasenna)
            .execute('spVal_usuario_login')

        const  data ={
            perfil: ret.recordsets[0][0].perfil,
            usuario: ret.recordsets[0][0].usuario,
            correo: ret.recordsets[0][0].correo,
            personal: ret.recordsets[0][0].personal,
            id_usuario: ret.recordsets[0][0].id_usuario
        }

        let token = await CreaToken(data)

        var validacion = {
            code: ret.recordsets[0][0].CODE,
            message: ret.recordsets[0][0].MESSAGE,
            token
        }

        res.status(200).json(validacion)
    } catch (err) {
        console.log(`error: ${err}`)
        res.status(500).json({ error: err })
    }
}

module.exports = {
    validaUsuario
}