var config = require('./dbconfig')
const sql = require('mssql')

async function consulta(){
    try {
        let pool = await sql.connect(config)
        let personal = await pool.request().query("SELECT * FROM PERSONAL")
        console.log(personal.recordsets);
        return personal.recordsets
    } catch (error) {
        console.log(`error: ${error}`);
    }
}


module.exports = {
    consulta
}