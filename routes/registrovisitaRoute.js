const express = require('express')
const router = express.Router()
const registro = require('../controllers/registrovisitasController')
const seguridad = require('../Seguridad')

router.post('/registrovisita/nuevo',seguridad.VerificarToken,registro.nuevo)
router.get('/registrovisita/listacard',seguridad.VerificarToken,registro.listaCard)
router.get('/registrovisita/busqueda',seguridad.VerificarToken,registro.registroVisitaInfo)

module.exports = router