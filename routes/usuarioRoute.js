const express = require('express')
const router = express.Router()
const controlador = require('../controllers/usuarioController')

router.post('/usuario/validarUsuario',controlador.validaUsuario)

module.exports = router