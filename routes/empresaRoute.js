const express = require('express')
const router = express.Router()
const controlador = require('../controllers/empresaController')
const seguridad = require('../Seguridad')

router.get('/empresa/listasimple',seguridad.VerificarToken,controlador.listaSimple)

module.exports = router