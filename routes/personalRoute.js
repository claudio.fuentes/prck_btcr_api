const express = require('express')
const router = express.Router()
const controlador = require('../controllers/personalController')
const seguridad = require('../Seguridad')

router.get('/personal/listasimple',seguridad.VerificarToken,controlador.listaSimple)

module.exports = router