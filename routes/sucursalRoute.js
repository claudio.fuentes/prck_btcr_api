const express = require('express')
const router = express.Router()
const controlador = require('../controllers/sucursalController')
const seguridad = require('../Seguridad')

router.get('/sucursal/listasimple',seguridad.VerificarToken,controlador.listaSimple)

module.exports = router