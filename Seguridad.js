const jwt = require('jsonwebtoken')
const config = require('./config.json')


CreaToken = async (data) => {
    return await jwt.sign(data, config.secPass,{expiresIn:config.timeToken})
}

VerificarToken = async(req,res,next)=>{
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
            const bearerToken = bearerHeader.split(' ')[1]
            let token = await jwt.verify(bearerToken,config.secPass)
            req.token = token
            next()
    }else{
        res.sendStatus(403)
    }
}

module.exports = {
    CreaToken,
    VerificarToken
}