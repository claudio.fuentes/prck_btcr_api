// DECLARACIÓN DE CONSTANTES Y VARIABLES
const express = require("express")
const app = express();
const dboperation = require("./dboperation")

const regVisitaRoute = require('./routes/registrovisitaRoute')
const empresaRoute = require('./routes/empresaRoute')
const personalRoute = require('./routes/personalRoute')
const sucursalRoute = require('./routes/sucursalRoute')
const usuarioRoute = require('./routes/usuarioRoute')

const cors = require('cors')
const morgan = require('morgan')

//MIDLEWARE
app.use(express.urlencoded({extended:"false"}))
app.use(morgan('dev'))
app.set('port',process.env.PORT || 3001)
app.use(express.json())
app.use(cors())


app.use('/',regVisitaRoute)
app.use('/',empresaRoute)
app.use('/',personalRoute)
app.use('/',sucursalRoute)
app.use('/',usuarioRoute)

// app.use(cors())



app.listen(app.get("port"),()=>{
    console.log(`bitacora listening on port ${app.get("port")}`);
})

app.get('/',(req,res)=>{
    res.send('<h1>Procheck</h1>')
})
